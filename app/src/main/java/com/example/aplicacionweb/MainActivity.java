package com.example.aplicacionweb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private WebView webView;
    private WebSettings myWebSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        webView = findViewById(R.id.webView);
        myWebSettings = webView.getSettings();
        myWebSettings.setJavaScriptEnabled(true);
        myWebSettings.setDomStorageEnabled(true);
        webView.loadUrl("https://scintillating-crumble-440441.netlify.app/");
        webView.setWebViewClient(new WebViewClient());
    }

    private void configureWebView() {
        // Configuración del WebView
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true); // Habilitar JavaScript (si es necesario)
        webView.setWebViewClient(new WebViewClient());

        // Cargar la página web externa
        webView.loadUrl("https://scintillating-crumble-440441.netlify.app/");
    }

    // Manejar el botón Atrás del dispositivo
    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}